# SETUP

## Prerequisites:
- [NVM](https://github.com/nvm-sh/nvm#installing-and-updating) or some other means for installing Node.js. **NOTE:** Use the version of Node which is listed in the file `.nvmrc`.
- [PostgreSQL](https://www.postgresql.org/download/)

## Prepare the Node project

1. Clone the project. 
2. In the project directory, run <kbd>nvm install</kbd> to install the appropriate version of Node for this project using NVM.
3. Run <kbd>npm install</kbd> to install this project's JavaScript dependencies using NPM.

## Prepare the database

1. Confirm your SQL server's hostname. This is probably "localhost" if you are running Postgres locally. In the Node server shell, set an environment variable of <kbd>PGHOST</kbd> to this hostname.
2. Create a database (e.g. "jservice_db"). For example, in the `psql` console:
```sql
CREATE DATABASE jservice_db;
```
3. In the Node server shell, set an environment variable of <kbd>PGDATABASE</kbd> to the database name.
4. Prepare a SQL user with the `CREATEDB` role attribute (e.g. "jservice_user").
   - You must also grant `PG_READ_SERVER_FILES` to this user, so that through this user, Postgres can access this project's CSV files during the seeding process below. Alternatively, the user must be a `superuser`.
   - For example, in the `psql` console:
```sql
CREATE USER jservice_user WITH CREATEDB;
GRANT PG_READ_SERVER_FILES TO jservice_user;
\password jservice_user
```
5. In the Node server shell, set an environment variable of <kbd>PGUSER</kbd> to the SQL username.
6. In your terminal, <kbd>cd</kbd> into this project's `seed` directory to prepare to run the local scripts for seeding the database.
7. We are ready to create the schema for this database. The `seed/create-schema.bash` script performs the work. It requires three commandline inputs (in order): the SQL server hostname (from Step 1), the database name (from Step 3), and the SQL username (from Step 5).
   - If this is your first time creating the schema for this database, you will see notices like `NOTICE: table "clues" does not exist, skipping`. This is expected.
```bash
jservice.xyz$ cd ./seed
jservice.xyz/seed$ ./create-schema.bash $PGHOST $PGDATABASE $PGUSER
```
1. We are ready to import the seed data into the new database. The `seed/import-data.bash` script performs this work. Just like the last one, this script requires three commandline inputs (in order): the SQL server hostname (from Step 1), the database name (from Step 3), and the SQL username (from Step 5).
```bash
jservice.xyz/seed$ ./import-data.bash $PGHOST $PGDATABASE $PGUSER
```

## Run the Node server

1. In your terminal, return to the root of the project (<kbd>cd ..</kbd>).
2. Start the server.
```bash
jservice.xyz$ npm start
```
3. Alternatively, start the development server.
```bash
jservice.xyz$ npm run dev
```