import fs from 'fs';
import Koa from 'koa';
import parser from 'koa-bodyparser';
import cors from '@koa/cors';
import route from 'koa-route';
import path from 'path';
import pg from 'pg';

import { categories } from './categories.mjs';
import { clues } from './clues.mjs';
import { games } from './games.mjs';

const PORT = process.env['PORT'] || 8182;
const app = new Koa();
const pool = new pg.Pool();

app.use(cors());
app.use(parser());

app.use(route.get('/', async ctx => {
  let readFile = async () => new Promise((good, bad) => {
    fs.readFile(path.join(process.cwd(), 'index.html'), 'utf8', (err, data) => {
      if (err) { return bad(err); }
      good(data);
    });
  });
  ctx.type = 'html';
  ctx.body = await readFile();
}));

app.use(route.get('/api/games', async ctx => await games.get(ctx, pool)));
app.use(route.get('/api/games/:id', async (ctx, id) => await games.getOne(ctx, id, pool)));
app.use(route.post('/api/random-games', async ctx => await games.createRandomGame(ctx, pool)));
app.use(route.get('/api/random-games/:id', async (ctx, id) => await games.getRandomGame(ctx, id, pool)));

app.use(route.get('/api/categories', async ctx => await categories.get(ctx, pool)));
app.use(route.get('/api/categories/:id', async (ctx, id) => await categories.getOne(ctx, id, pool)));
app.use(route.post('/api/categories', async (ctx) => await categories.post(ctx, pool)));

app.use(route.get('/api/clues', async ctx => await clues.get(ctx, pool)));
app.use(route.get('/api/clues/:id', async (ctx, id) => await clues.getOne(ctx, id, pool)));
app.use(route.put('/api/clues/:id', async (ctx, id) => await clues.put(ctx, id, pool)));
app.use(route.post('/api/clues', async ctx => await clues.post(ctx, pool)));
app.use(route.delete('/api/clues/:id', async (ctx, id) => await clues.destroy(ctx, id, pool)));
app.use(route.get('/api/random-clue', async ctx => await clues.getRandom(ctx, pool)));

app.listen(PORT, () => console.log(`Node server listening on port ${PORT}...`));
